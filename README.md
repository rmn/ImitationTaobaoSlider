# ImitationTaobaoSlider

ImitationTaobaoSlider(又称“仿淘宝滑块”)，这是一款jQuery仿淘宝拖动滑块验证码代码。

## 不足之处

1、这里只实现了仿淘宝滑块特效；
2、快速拉动“仿淘宝滑块”会出现bug；
3、没有存放防卫机制算法；

## 提示

仅供参考，谢谢合作！！